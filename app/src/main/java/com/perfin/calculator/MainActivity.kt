package com.perfin.calculator

import android.R
import android.os.Bundle
import android.view.WindowManager
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.perfin.calculator.databinding.ActivityMainBinding
import org.w3c.dom.Text


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
        init()

    }

    fun init(){

        binding.btnCong.setOnClickListener {
            val soA = binding.soA.text.toString().toFloat()
            val soB = binding.soB.text.toString().toFloat()
            binding.btnKq.text = (soA+ soB).toString()

        }
        binding.btnTru.setOnClickListener {
            val soA = binding.soA.text.toString().toFloat()
            val soB = binding.soB.text.toString().toFloat()
            binding.btnKq.text = (soA - soB).toString()
        }
        binding.btnNhan.setOnClickListener {
            val soA = binding.soA.text.toString().toFloat()
            val soB = binding.soB.text.toString().toFloat()
            binding.btnKq.text = (soA* soB).toString()
        }
        binding.btnChia.setOnClickListener {
            val soA = binding.soA.text.toString().toFloat()
            val soB = binding.soB.text.toString().toFloat()
            binding.btnKq.text = (soA/ soB).toString()
        }
    }

}